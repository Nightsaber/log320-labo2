import java.io.FileInputStream;
import java.util.ArrayList;



public class ReadFileGrille {
	
	private static final int NEW_LINE = 10;
	private static final int CARRIAGE_RETURN = 13;
	private static final int CHAR_TO_INT = 48;
	
	private static int[][] grille = {
	        {0,0,0,0,0,0,0,0,0},
	        {0,0,0,0,0,0,0,0,0},
	        {0,0,0,0,0,0,0,0,0},
	        {0,0,0,0,0,0,0,0,0},
	        {0,0,0,0,0,0,0,0,0},
	        {0,0,0,0,0,0,0,0,0},
	        {0,0,0,0,0,0,0,0,0},
	        {0,0,0,0,0,0,0,0,0},
	        {0,0,0,0,0,0,0,0,0},
	};

	public static int[][] readFile(String filePath){

		try {
			FileInputStream inputStream = new FileInputStream(filePath);

			int ligne = 0;
			int colonne = 0;
			
			int candidat;		
			while((candidat = inputStream.read()) != -1){
				if(candidat != NEW_LINE && candidat != CARRIAGE_RETURN)
					grille[ligne][colonne] = candidat - CHAR_TO_INT;
				if(colonne > 9){
					ligne++;
					colonne = 0 ;
				}
				else
					colonne++;
			}
			inputStream.close();

			return grille;
		} catch (Exception e) {
			e.printStackTrace();
			return grille;
		}
	}
}
