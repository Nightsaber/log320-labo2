import java.text.Collator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;


/**
 * cette classe permet de cr�er la vrai grille de sudoku
 * elle va chercher la grille de format string pour la transformer en format int
 * elle contient aussi des accesseur et des mutateurs pour manipuler la grille�
 * ainsi que plusieurs methodes pour verifier la validite de la grille
 * 
 * @author Pierre-Luc Cusson & Simon Begin
 * @since 28 mai 2015 
 */


public class GrilleSudoku {


	/**
	 * Cr�ation des 3 tableaux qui vont �tre utilis� pour stocker les
	 * valeurs
	 */
	private static boolean[][] tableauLigne = new boolean[9][9];
	private static boolean[][] tableauCol = new boolean[9][9];
	private static boolean[][] tableauRegion = new boolean[9][9];
	
	
	/**
	 * Permet d'afficher la grille
	 * et valide la grille
	 */
	public static void affichage(int[][] grille) {
		for (int ligne = 0; ligne < 9; ligne++) {
			for (int col = 0; col < 9; col++) {
				System.out.printf("%d ", grille[ligne][col]);
			}
			System.out.printf("\n");
		}
		System.out.printf("\n");
	}
	
	/**
	 * On construit les tableaux avec les valeurs existantes dans la grille
	 * En somme, si on a une valeur x (-1 car le talbeau va jusqu'� 8 (0 � 8)) dans la grille � une position on ins�re la ligne ou col ou la region
	 * ensuite sa valeur et true ou false pour dire qu'il a une valeur � cette endroit.
	 * 
	 * Pour 3*ligne/3+(col/3) voir le pdf du laboratoire.
	 * 
	 */
	public static void constructionTableaux(int[][] grille) {
		int valeur;
		
		// Initialisation des tableaux
		for (int i = 0; i < 9; i++) {
			for (int j = 0; j < 9; j++) {
					tableauLigne[i][j] = false;
					tableauCol[i][j] = false;
					tableauRegion[i][j] = false;
				} 
			}
		// Insertion des valeurs
		for (int ligne = 0; ligne < 9; ligne++) {
			for (int col = 0; col < 9; col++) {
				valeur = grille[ligne][col];
				if (valeur != 0) {
					tableauLigne[ligne][valeur - 1] = true;
					tableauCol[col][valeur - 1] = true;
					tableauRegion[3*(ligne/3)+(col/3)][valeur - 1] = true;
				}
			}
		}
	}
	

	/**
	 * 
	 * Ins�re une valeur et puis valide le r�sultat sinon, recule.
	 * 
	 */
	public static boolean estValide(int grille[][], ArrayList<Node> liste, int position){
		
		// fin du sudoku
		if (position >= liste.size()) {
			return true;
		}
		
		// R�cup�re dans la liste la position
		Node node = liste.get(position);

		int ligne = node.ligne;
		int col = node.col;
		
		for (int i=0; i <9; i++) {
			// Si la valeur n'est pas un doublon dans nos tableaux
			if (!tableauLigne[ligne][i] && !tableauCol[col][i] && !tableauRegion[3*(ligne/3)+(col/3)][i]) {
				
				// Pas un doublon donc on ins�re la valeur dans nos tableau
				tableauLigne[ligne][i] = true;
				tableauCol[col][i] = true;
				tableauRegion[3*(ligne/3)+(col/3)][i] = true;
				
				// Ins�re le r�sultat dans la grille
				if (estValide(grille, liste, position + 1)) {
					grille[ligne][col] = i + 1;
					return true;
				}
				
				// Blocage, donc on recommence avec une autre valeur et donc on supprime cette valeur dans nos tableaux
				tableauLigne[ligne][i] = false;
				tableauCol[col][i] = false;
				tableauRegion[3*(ligne/3)+(col/3)][i] = false;
			}
		}
		
		return false;
		
	} // fin estValide()
	
	/**
	 * Nombre de valeurs possibles pour une case vide
	 * 
	 * @return nombre de valeurs possibles pour une case vide
	 */
	public static int possibilite(int grille[][], int ligne, int col) {
		int valeurPossible = 0;
		
		for (int i = 0; i < 9; i++) {
			if (!tableauLigne[ligne][i] && !tableauCol[col][i] && !tableauRegion[3*(ligne/3)+(col/3)][i]) {
				valeurPossible++;
			}
		}
		return valeurPossible;
	} //fin possibilite()
	
	public static boolean resoudre(int grille[][]) {
		ArrayList<Node> liste = new ArrayList<Node>();
		
		// On construit les tableaux
		constructionTableaux(grille);
		
		// Contruit notre liste avec le nombre de possibilit� pour chaque case
		for (int ligne = 0; ligne < 9; ligne++) {
			for (int col = 0; col < 9; col++) {
				if (grille[ligne][col] == 0) {
					liste.add(new Node(ligne, col, possibilite(grille, ligne, col)));
				}
			}
		}
		
		// Trie la liste avec le nombre de possibilit�s pour permettre un meilleur backtracking
		// Donc �a permet de mettre les valeurs sur les cases o� il y a moins de possibilit� pour
		// ensuite aller vers les cases avec plus de possibilit�.
		// Donc, on obtient un backtracking plus efficace.
		Collections.sort(liste, new Comparator<Node>() {
			@Override
			public int compare(Node node1, Node node2) {
				if (node1.valeurPossible < node2.valeurPossible) {
					return -1;
				}
				if (node1.valeurPossible > node2.valeurPossible) {
					return 1;
				}
				return 0;
			}
		});
		
		return estValide(grille, liste, 0);
		
	}


}//fin de GrilleSudoku