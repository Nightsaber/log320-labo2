
public class Node {
		int ligne;
		int col;
		int valeurPossible;
		
		public Node(int ligne, int col, int valeurPossible) {
			this.ligne = ligne;
			this.col = col;
			this.valeurPossible = valeurPossible;
		}
}
