/**
 * cette classe contient le main pour solutionner la grille sudoku,
 * et elle va solutionner la grille
 * pour ensuite afficher la grille solutionne
 * 
 * @author Pierre-Luc Cusson (CUSP24049009)
 * @author Simon Begin
 * @since juin 2015
 */
public class Main {
       

    public static void main(String[] args) {    	

    	//contient la grille sudoku a solutionner
       int[][] grille = ReadFileGrille.readFile(args[0]);

        System.out.println("Grille Sudoku initiale:");
        GrilleSudoku.affichage(grille);     
        
    	long start = System.currentTimeMillis();
        boolean tmp = GrilleSudoku.resoudre(grille);
        long end = System.currentTimeMillis();
        
        //afficher la grille de sudoku solutionnee ou non
     
        if (tmp) {
        System.out.println("Grille Sudoku final:");
        GrilleSudoku.affichage(grille);
        } else {
        	System.out.println("Grille non valide");
        	System.out.println("\n");
        }
        
        System.out.println("temps d'execution: "+(end - start)+" millisecondes");
    }
}